#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 20:08:57 2021

@author: charlievidal
"""
import pandas as pd
import numpy as np
import scipy.signal as scy
import scipy.stats as stats
import matplotlib.pylab as plt
import itertools
import more_itertools as mit
from getAPDMdata import getAPDMdata
import collections




# Determining Gait

class gait_extraction():
    
    def __init__(self,experiment):
        
        self.trials = experiment.trials
        self.record_length = experiment.record_length 
        self.fs = experiment.fs
        self.threshold = []
        
    
    def load_data(self,experiment):
        
        """
        Loads the data from specific instance of an experiment
        
        Parameters:
        --------
        experiment: Instance of class experiment
            
        Returns:
        --------
        data : Pandas DataFrame 
                data = {'record':[],  'sensor':[],
                        'accel':[], 'time':[]}
        """
        
        # Creating dictionary
        data = {'record':[], 'sensor':[], 'accel':[], 'time':[]}
        
        
        # Creating a specific paramters DataFrame
        df = pd.read_hdf(experiment.fname, 'experiment/specific_parameters')
        
        # Checking for more than one record
        record = df['id'].unique()
        
        # Checking all trials 
        for t in range(len(self.trials)):
                
            # Looping through all sensors
            for s in range(np.size(self.trials[t].get_data(),axis=1)):
                
                data['record'].append(record[t])
                data['sensor'].append(s)
                data['accel'].append(self.trials[t].get_data()[:,s])
                data['time'].append(np.arange(0,self.record_length,1/self.fs))
        
        data = pd.DataFrame(data)
        
        return data
    
    def combine_data(self,data):
        
        """
        Checks all acceleration records and super imposes them based on the 
        highest energy.
        
        Parameters
        ----------
        data : Pandas DataFrame 
                data = {'record':[],  'sensor':[],
                        'accel':[], 'time':[]}
                
        Returns:
        --------
        data : Pandas DataFrame 
                data = {'record':[], 'accel':[], 'time':[]}
        """
        
        d = {'record':[], 'accel':[], 'time':[]}
        
        # Getting all data from a specific record
        for record_i in data['record'].unique():
            
            temp_data = data['accel'][data['record'] == record_i]
            new_data = []
            
            # Getting the individual data point index
            for j in range(len(temp_data[temp_data.index[0]]+1)):
            
                values = []    
                signs = []
            
                # Getting the index of the individual sensor
                for i in range(temp_data.index[0],temp_data.index[-1]+1):
                    
                    values.append(abs(temp_data[i][j]))
                    signs.append(np.sign(temp_data[i][j]))
                    
                ind = np.argmax(values)
                
                new_data.append(values[ind]*signs[ind])
                
            d['accel'].append(np.array(new_data))
            d['record'].append(record_i)
            d['time'].append(data['time'][data['record']==record_i][data['time'][data['record']==record_i].index[0]])
            
        return pd.DataFrame(d)
    
    def wfilter(self,data,mysize=201):
        
        """
        Filters the data using the wiener filter
        
        Source: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.wiener.html
        
        Parameters
        ----------
        data : pandas Data Frame 
                data = {'record':[],  'sensor':[],
                            'accel':[], 'time':[]}
                
        mysize:  int
                The size of the wiener filter window, Default is 201.
            
        Returns
        -------
        w_filter: pandas Data Frame 
                data = {'record':[],  'sensor':[],
                            'accel':[], 'time':[]}
                
                The filtered data is contained in 'accel'
        
        """
        
        # Applying the function in a vectorized format
        data['accel'] = data.apply(lambda row: scy.wiener(row['accel'],mysize=mysize),axis=1)
            
        return data
    
    def getThreshold(self,data,time,threshold_value):
        
        """
        Gets the threshold of a given signal based on a desired wait time.
        
        Parameters
        ----------
        data: array 
                contains acceleration data.
                
        time: int
                desired time in seconds to calculate the threshold.
                
        threshold_value: bool
                if the threshold value = True then the user will select the 
                desired time for which to calculate the threshold. If the 
                threshold value = False then the threshold is automatically
                calculated based on where the data first exceeds the mean.
                
        Returns
        -------
        index: float64 
                contains the index of the calculated threshold.
        
        """
        if threshold_value == True:
            
            section = int(np.round(time*self.fs))
            
            ind = np.argmax(data[:section])
            
        else:
        
            # end = np.where(data > np.mean(abs(data)))[0][0]
            
            # ind = np.argmax(data[:end-1])
            no_noise_section = np.where(data < np.mean(abs(data)))[0]
            
            cons_values = [list(group) for group in mit.consecutive_groups(no_noise_section)]
            
            max_ind = max(cons_values, key=len)
            
            section = int(np.round(len(max_ind)/2))
            
            quarter_section = int(np.round(section/2))
            
            ind = np.argmax(data[max_ind[quarter_section]:max_ind[quarter_section+section]])+max_ind[quarter_section]
            
            # ind = np.argmax(data[max_ind[1]:max_ind[-1]])+max_ind[1]
            
            # len_cons = [np.size(cons_values[i]) for i in range(len(cons_values))]
            
            # max_ind = np.argmax(len_cons)
            
            # ind = np.argmax(data[cons_values[max_ind][1]:cons_values[max_ind][-1]])+cons_values[max_ind][1]
        
        return abs(data[ind])
        
    
    def getConsecutive(self,signal):
        
        """
        Gets the index of consecutive numbers in a list.
        
        Parameters
        ----------
        signal: list
                contains the signal of acceleration data, 0's and 1's which 
                represent a value greater than (1) or less than (0) the 
                calculated threshold.
        
        Returns
        -------
        cons: list
                contains a list of only consecutive numbers.
        
        """
        
        # Getting the index of signals > 0 
        ind = []
        
        for i in range(len(signal)):
            
            if signal[i] == 1:
                
                ind.append(i)
         
        # Getting the consecutive values for each signal > 0
        cons = [list(group) for group in mit.consecutive_groups(ind)]
        
        return cons
    
    def remove_non_windows(self,cons_index):
        
        """
        Gets all values which do not pertain to a window. 
        For example, a single number window cannot exist and a window which
        is only two points is not a true window.
        
        Parameters
        ----------
        cons_index : list
                contains a list of consecutive value indexes (windows) which 
                may contain singular values.

        Returns
        -------
        vals_to_remove : list
                contains a list of singular value indexes to remove.

        """
        
        vals_to_remove = []
        
        for i in cons_index:
            
            if len(i) <= 75:
                
                vals_to_remove.append(i)
                
        return vals_to_remove
    
    def get_step_window(self,signal):
        
        """
        Creates window for detected step events only based on consecutive 
        values. The windows are generated by checking if the next window is 
        within 50 points of the previous window, if not it is assumed that the 
        next window is not a contribution of the overall event window.
        
        Parameters
        ----------
        signal: list
                contains the signal of acceleration data, 0's and 1's which 
                represent a value greater than (1) or less than (0) the 
                calculated threshold.

        Returns
        -------
        total_ind : list
                containes first and last values of indexes (windows) for which 
                the acceleration data is greater than the calculated threshold.
        
        """
        
        # Getting consecutive values
        cons = self.getConsecutive(signal)
                
        # Creating a window 
        total_ind = []
        count=0
        
        # Checking to see if the windows are close to each other
        for i in range(len(cons)-1):
            
            if i==0:
                
                if cons[i+1][0] - cons[i][-1] < int(np.round(0.03*self.fs)):
                    
                    total_ind.append(cons[i]+cons[i+1])
            
            else:
                
                if len(total_ind) > 0:
                
                    if cons[i+1][0] - total_ind[count][-1] < int(np.round(0.03*self.fs)):
        
                        total_ind[count] = (total_ind[count]+cons[i+1])
        
                    else:
                        
                        count += 1
                        total_ind.append(cons[i+1])  
                else:
                    
                    if cons[i+1][0] - cons[i][-1] < int(np.round(0.03*self.fs)):
                        
                        total_ind.append(cons[i]+cons[i+1])
        
        # Removing single windows
        val_to_remove = self.remove_non_windows(total_ind)
        
        for i in val_to_remove:

            total_ind.remove(i)
        
        # Getting the first and last index of window
        for i in range(len(total_ind)):
            
            total_ind[i] = [total_ind[i][0],total_ind[i][-1]]
        
        return total_ind
    
    def get_moving_window(self,data,window):
        
        """
        Creates a forward moving window based on input data
        
        """
        
        d = []
        
        for i in range(len(data)-window-1):
               
            d.append(data[i:i+window])
                
        return d
    
    def get_static_window(self,data,window):
        
        """
        Creates a static window based on input data
        
        """
        d = []
        
        inc = list(range(0,len(data)+window,window))
        
        for i in range(len(inc)):
             
            if i <= len(inc)-2:
                
                d.append(data[inc[i]:inc[i+1]])

        return d
    
    def getRMS(self,data):
        
        """
        Calculates the root-mean-square for a desired array of data.
        
        """
        
        d = []
        
        for i in range(len(data)):
            
            d.append(np.sqrt(np.mean(data[i]**2)))

        return d 
    
    def getRatios(self,rms,type='static'):
        
        """
        Calculates the signal to noise ratio given the RMS of desired data.
        
        """
        
        r = []
        
        for i in range(len(rms)):
            
            if i == 0:
                
                ratio = rms[i]
                r.append(ratio)
                
            else:
               
                ratio = rms[i]/rms[i-1]
                r.append(ratio)
                
        return r
    
    def signal2noise(self,data,window,window_type='static'):
        
        """
        Generates a signal to noise ratio based on a desired window size. This is
        can be used for plotting purposes as well. 
        
        """
        
        # checking to see if the window size is appropriate
        # if not (len(data)/window).is_integer():
                
        #     raise Exception('Please select a window size which produces a whole number when divisible by the length of the data')
        
        if window_type=='static':
            
            sections = self.get_static_window(data,window)
            
            rms = self.getRMS(sections)
            
            ratios = self.getRatios(rms)
            
            snr = np.repeat(ratios,window+1)
            
            return snr
            
        else:
            
            sections = self.get_moving_window(data,window)
            
            rms = self.getRMS(sections)
            
            ratio_f = self.getRatios(rms)
#            rms.reverse()
#            ratio_b = self.getRatios(rms)
            
#            snr = []
            sections = []
            
            for i in range(len(ratio_f)):
                
#                snr.append(list(range(i,window+i)))
                sections.append(np.repeat(ratio_f[i],window))
                
            return sections
    
    def get_SNR(self,data,window,window_type='static'):
        
        """
        Calculates the SNR for every row in the input Data Frame based on function
        signal2noise.
        
        """
        
        # include the iterations here for vectorization
        if window_type != 'static':
        
            data['snr'] = data.apply(lambda row: self.signal2noise(row['accel'],window,window_type),axis=1)

        else:
            
            data['snr'] = data.apply(lambda row: self.signal2noise(row['accel'],window,window_type),axis=1)
        
        return data
    
    def get_signal(self,data,threshold_t=3,threshold_value=True):
        
        """
        Creates a signal for every point at or above the desired calculated 
        threshold. The signal value is either 0 if the absolute value of the 
        data point is at or below the threshold and 1 if it is above the 
        threshold.
        
        Parameters:
        --------
        data: array 
                contains acceleration data
        
        threshold_t: int
                the desired time that the threshold will be calculated from. The
                default is 3.
                Example: threshold_t=3 calculates the threshold using the first
                3 seconds of the data.
                
        Returns:
        --------
        signal: list
                contains values of 0's and 1's which represent a value greater 
                than the threshold (1) and less than the threshold (0).
        
        """
        
        signal = np.zeros(len(data))
        threshold = self.getThreshold(data,threshold_t,threshold_value)
        
        for i in range(len(data)):
            
            if data[i] <= threshold:
                
                signal[i] = 0
            
            else:
            
                signal[i] = 1
    
        return list(map(int,signal))
    
    def check_signal(self,data,signal,threshold_t=3,threshold_value=True):
        
        if sum(i > 0 for i in signal) > e.fs:
            
            wdata = scy.wiener(data,mysize=201)
            
            signal = self.get_signal(wdata,threshold_t,threshold_value)
            
        else:
            
            signal = signal
            
        return signal
    
    def normalize(self,data,snr):
        
        """
        Normalizes the signal to noise ratio to that of the acceleration data 
        based on the peak acceleration as well as detrends the data to zero mean.
        
        """
        
        # Finding the max values
        ymax = np.max(data)
        smin = np.min(snr)
        smax = np.max(snr)
        
        # Appending the new values
        ynorm = [ymax*(snr-smin)/(smax-smin)]
        
        return ynorm
    
    def clean_snr(self,data,snr,threshold_t=3):
        
        """
        Creates a clean version of the SNR windows by taking away small SNR windows 
        depending on the mean value of the data.
        
        """
        threshold = self.getThreshold(data,threshold_t)
        
        for i in range(len(snr)):
            
            if snr[i] <= threshold:
                
                snr[i] = 0
                
            else:
                
                snr[i] = snr[i]
                
        return snr
    
    def getOutlier(self,data):
        
        """
        Gets the outlier of data based on IQR method.
        
        Source: https://online.stat.psu.edu/stat200/lesson/3/3.2
        
        Parameters
        ----------
        data: list 
                contains values for which to generate outliers from.

        Returns
        -------
        outliers: list
                contains outlier values
        
        """
        abs_data = [abs(x) for x in data]
        
        Q1 = np.quantile(abs_data,0.25)
        Q3 = np.quantile(abs_data,0.75)
        IQR = Q3-Q1
        
        lower_limit = Q1
        
        upper_limit = Q3 + 2*IQR

        outliers = []
        
        for i in range(len(abs_data)):
            
            
            if abs_data[i] <= lower_limit or abs_data[i] >= upper_limit:
                
                outliers.append(data[i])
        
        return outliers
    
    def duplicates(self,n):
        
        """
        returns the maximum list of duplicates 
        
        
        """
        
        counter = collections.Counter(n)
        
        dups = [i for i in counter if counter[i]!=1]
        
        result = {}
        
        for item in dups:
            
            result[item] = [i for i,j in enumerate(n) if j==item]

        if len(result) < 1:
            
            result = [0]
        
        else:
            
            result = result[max(result, key= lambda x: len(set(result[x])))]
        
        return result
        
    
    def get_peak_window(self,data,signal):
        
        """
        Analyzes peaks based on threshold windows. This function takes into 
        account false peaks by using a modified version of the IQR outlier 
        method.
        
        Parameters
        ----------
        data: array 
                contains acceleration data.
        
        signal: list
                contains the signal of acceleration data, 0's and 1's which 
                represent a value greater than (1) or less than (0) the 
                calculated threshold.

        Returns
        -------
        windows_used: list
                contains the first and last index of the windows used without
                the outliers.
        
        peak_values: list
                contains the acceleration peak values found inside the generated
                window.
                
        """
        
        # Getting the window indexes
        window = self.get_step_window(signal)
        
        peak_ind = []
        
        for i in range(len(window)):
            
            peak_ind.append(np.argmax(abs(data[window[i][0]:window[i][1]]))+window[i][0])
            
        if len(peak_ind) < 1:
            
            windows_used = 'please check data, could not calculate cadence'
            peak_ind_used = 'please check data, could not calculate cadence'
            
        else:
            
            peak_values = []
            
            for i in range(len(peak_ind)):
                
                peak_values.append(data[peak_ind[i]])
            
            # NEW STUFF
            dist = [peak_ind[i+1] - peak_ind[i] for i in range(len(peak_ind) - 1)]
            
            if len(dist) < 1:
                
                windows_used = 'please check data, could not calculate cadence'
                peak_ind_used = 'please check data, could not calculate cadence'
                
            else:
            
                first_dig = []
                
                len_dist = []
                
                # Checking for similarities in numbers
                for i in range(len(dist)):
                    
                    first_dig.append(int(str(dist[i])[0]))
            
                    len_dist.append(len(str(dist[i])))
                    
                cons_fd = self.duplicates(first_dig)
    
                cons_ld = self.duplicates(len_dist)
                
                dist_idx = list(set(cons_fd).intersection(cons_ld))
                
                if len(dist_idx) < 1:
                    
                    windows_used = 'please check data, could not calculate cadence'
                    peak_ind_used = 'please check data, could not calculate cadence'
                
                else:
                    
                    peak_ind_used = peak_ind[dist_idx[0]:dist_idx[-1]+1]
                    
                    windows_used =  window[dist_idx[0]:dist_idx[-1]+1]
                
            # # VARIATION CODE HERE
            # l_var = []
            
            # wind = []
            
            # # Checking the variation between distances
            # for i in range(round(len(dist)/2)):
                
            #     if i == 0:
                    
            #         temp_dist = dist
                    
            #         l_var.append(stats.variation(temp_dist))
                    
            #     else:
                
            #         temp_dist = dist[i:-i]
                    
            #         l_var.append(stats.variation(temp_dist))
                    
            #         if l_var[i] <= 0.20:
                        
            #             wind.append(temp_dist)
            
            # start_ind = dist.index(wind[0][0])
            
            # end_ind = dist.index(wind[0][-1])
            
            # peak_ind_used = peak_ind[start_ind:end_ind+1]
                
            # windows_used =  window[start_ind:end_ind+1]   
            
        
            # # CONSISTANCY CODE HERE
            # # ranges for consistancy 
            # Q1 = np.percentile(dist,10)
            # Q3 = np.percentile(dist,90)
            
            # dist_ind = []
            # for i in range(len(dist)):
                
            #     if dist[i] > Q1 and dist[i] < Q3:
                    
            #         dist_ind.append(i)
            
            # cons = [list(group) for group in mit.consecutive_groups(dist_ind)]
            
            # # Grabbing the maximum consistant steps
            # if len(cons) < 1:
                
            #     windows_used = 'please check data, could not calculate cadence'
            #     peak_ind_used = 'please check data, could not calculate cadence'
                
            # else:
                
            #     max_cons = max(cons, key=len)
                
            #     windows_used = []
            #     peak_ind_used = []
            #     for i in max_cons:
            #         windows_used.append(window[i])
            #         windows_used.append(window[i+1])
            #         peak_ind_used.append(peak_ind[i])
            #         peak_ind_used.append(peak_ind[i+1])
                    
            #     windows_used.sort()
            #     windows_used = list(k for k,_ in itertools.groupby(windows_used))
                
            #     peak_ind_used.sort()
            #     peak_ind_used = list(k for k,_ in itertools.groupby(peak_ind_used))
            
        # # Determining and removing outliers
        # outliers = self.getOutlier(peak_values)
        
        # for i in outliers:
            
        #     peak_values.remove(i)
            
        # windows_used = []
        
        # for i in range(len(window)):
            
        #     for j in range(len(peak_values)):
        
        #         if np.where(data==peak_values[j])[0][0] >= window[i][0] and np.where(data==peak_values[j])[0][0] <= window[i][1]:
                    
        #             windows_used.append(window[i])   

        # peak_ind = []
        
        # for i in range(len(windows_used)):
            
        #     peak_ind.append(np.argmax(abs(data[windows_used[i][0]:windows_used[i][1]]))+windows_used[i][0])
        
        # return windows_used , peak_values, peak_ind
        
        return windows_used, peak_ind_used
    
    
    
    def get_trace(self,data,signal):
        
        """
        Gets the series of steps in the data for analyzing. The step events 
        must be no less than one fifth of a second apart according to sources:
        https://www.sciencedirect.com/topics/nursing-and-health-professions/stride-time
        https://www.econathletes.com/post/math-for-sprinters-steps-per-second-and-stride-length
        
        If the step events are less than one fifth of a second apart, it
        is assumed to be a false peak, noise, or in general an outlier.
        
        The step events must also not be greater than one second apart. If the
        step events are greater than one second apart, they are assumed to not 
        be a part of a trace, a start to a different trace, or in general an
        outlier.
        
        Parameters
        ----------
        data: array 
                contains acceleration data.
            
        signal: list
                contains the signal of acceleration data, 0's and 1's which 
                represent a value greater than (1) or less than (0) the 
                calculated threshold.
        
        Returns:
        --------
        trace: list
                contains the first and last index of windows which contain a 
                series of steps. 
        
        peak_values: list
                contains the absolute maximum acceleration value in a window.
        
        """
        
        windows, peak_values, peak_ind = self.get_peak_window(data,signal)
        
        dist = [windows[i+1][0] - windows[i][1] for i in range(len(windows) - 1)]

        # First Iteration
        trace = []
        
        for i in range(len(dist)):

            if dist[i] > self.fs/5 and dist[i] < self.fs:

                trace.append(windows[i])
                trace.append(windows[i+1])
        
        trace.sort()
        trace = list(k for k,_ in itertools.groupby(trace))
        
        peak_values = []
        
        for i in range(len(trace)):
            
            peak_values.append(data[np.argmax(abs(data[trace[i][0]:trace[i][1]]))+trace[i][0]])
            
        # Checking the length of each trace for validation
        # if a window is less than 1/50th of a second, it is not a step event
        real_trace = []
        wind_size = [trace[i][1] - trace[i][0] for i in range(len(trace))]
        
        for i in range(len(wind_size)):
            
            if wind_size[i] > self.fs/50:
            
                real_trace.append(trace[i])
                
        return real_trace, peak_values
    
    def get_avg_cadence(self,data,signal,time):
        
        """
        Determins the cadence (walking frequency) in steps/minute based on
        acceleration data by averaging the cadence for every three steps.
        
        Parameters
        ----------
        data: array 
                data containing acceleration records.
            
        signal: list
                signal windows containing 0's and 1's for a desired data array.
        
        time: array
                time vector created when loading data.
        
        Returns:
        --------
        cadence: int or string
                the calculated cadence for each acceleration record. If the 
                cadence cannot be calculated, a string will be produced saying
                so.
        
        """
        trace, peak_ind = self.get_peak_window(data, signal)
        
        if isinstance(trace,str):
            
            cadence = trace
            
        else:
            
            if len(trace) == 0:
                
                cadence = 'cannot calculate cadence'
                
            else:
                
                ts_cadence = []
                avg_cadence = []
                
                # Cumulative steps
                # for i in range(1,len(peak_ind)):
                    
                #     steps = i
                    
                #     t_end = time[peak_ind[i]]
                    
                #     t_start = time[peak_ind[0]]
                    
                #     ts_cadence.append((steps)/(t_end-t_start)*60)
                
                # # 25% of data
                # ten_p = int(np.round(len(ts_cadence)*.25))
                
                # cadence = np.median(ts_cadence[ten_p+1:len(ts_cadence)-ten_p])
                
                # If we choose incrementing 3 steps SMW (Best Performing Method)
                for i in range(len(trace)-3):
                     
                    steps = 3
                        
                    t_end = time[peak_ind[i+3]]
                        
                    t_start = time[peak_ind[i]]
                        
                    ts_cadence.append((steps/(t_end-t_start))*60)
                
                cadence = np.median(ts_cadence)
                
                # If we choose individual 3 steps FMW
                # for i in range(len(trace)):
                    
                #     if i == 0:
                        
                #         steps = 3
                        
                #         t_end = time[peak_ind[3]]
                        
                #         t_start = time[peak_ind[0]]
                        
                #         ts_cadence.append((steps/(t_end-t_start))*60)
                    
                #     elif i % 3 == 0 and i <= len(trace)-4:
                     
                #         steps = 3
                            
                #         t_end = time[peak_ind[i+3]]
                            
                #         t_start = time[peak_ind[i]]
                            
                #         ts_cadence.append((steps/(t_end-t_start))*60)
                
                # # 25% of data
                # ten_p = int(np.round(len(ts_cadence)*.25))
                
                # cadence = np.median(ts_cadence[ten_p+1:len(ts_cadence)-ten_p])
                
                # If we move inwards, the reverse of the cumulative method
                # steps = len(peak_ind)-1
                
                # for i in range(len(peak_ind)):
                    
                #     if i == 0: 
                        
                #         t_end = time[peak_ind[len(peak_ind)-1-i]]
                        
                #         t_start = time[peak_ind[i]]
                        
                #         ts_cadence.append((steps/(t_end-t_start))*60)
                        
                #     elif i > 0:
                        
                #         steps -= 2
                        
                #         t_end = time[peak_ind[len(peak_ind)-1-i]]
                        
                #         t_start = time[peak_ind[i]]
                        
                #         ts_cadence.append((steps/(t_end-t_start))*60)
                        
                # cadence = np.median(ts_cadence)
                
                # Determining the avergae cadence values
                # for i in range(len(trace)-3):
                    
                #     steps = 3
                    
                #     t_end = time[peak_ind[i+3]]
                    
                #     t_start = time[peak_ind[i]]
                    
                #     if i == 0:
                        
                #         ts_cadence.append((steps/(t_end-t_start))*60)
                #         avg_cadence.append((steps/(t_end-t_start))*60)
                        
                #     else:
                        
                #         ts_cadence.append((steps/(t_end-t_start))*60)
                #         avg_cadence.append(np.average([ts_cadence[i],avg_cadence[i-1]]))
                
                return ts_cadence
 
        # return cadence


    def get_cadence(self,data,signal,time):
                
        """
        Determins the cadence (walking frequency) in steps/minute based on 
        acceleration data by taking all steps divided by the change in time.
        
        Parameters
        ----------
        data: array 
                data containing acceleration records.
            
        signal: list
                signal windows containing 0's and 1's for a desired data array.
        
        time: array
                time vector created when loading data.
        
        Returns:
        --------
        cadence: int or string
                the calculated cadence for each acceleration record. If the 
                cadence cannot be calculated, a string will be produced saying
                so.
        
        """
            
        # trace, peak_values = self.get_trace(data,signal)
            
        trace, peak_ind = self.get_peak_window(data, signal)
        
        if isinstance(trace,str):
            
            cadence = trace
        
        else:
        
            if len(trace) == 0:
                
                cadence = 'cannot calculate cadence'
            
            else:
                steps = len(trace) 
                
                t_end = time[peak_ind[-1]]
                
                t_start = time[peak_ind[0]]            
    
                # t_end = time[np.where(data==peak_values[-1])[0][0]]
                
                # t_start = time[np.where(data==peak_values[0])[0][0]]
            
                # steps/second
                cadence = steps/(t_end-t_start)    
        
                # steps/minute
                cadence = cadence*60
        
        return cadence
        

#%% Work is calculated here
from experiment import Experiment
#%% Loading information/calculating
e = Experiment()
# e.load_setup('setup/walking_test_layout_1.json')

e.load_setup('setup/apdm_data.json')

ge = gait_extraction(e)

# data = ge.load_data(e)

data = getAPDMdata()

data = data[80:260]

data = ge.wfilter(data)

data = ge.combine_data(data)

# data = data.drop(data.index[60:])

# Using filtered once data
data['signal'] = data.apply(lambda row: ge.get_signal(row['accel'],threshold_t=3,threshold_value=False),axis=1)

# Using filtered 2x data
data['signal'] = data.apply(lambda row: ge.get_signal(scy.wiener(row['accel'],mysize=201),threshold_t=0.4,threshold_value=False),axis=1)

# Using filtered 3x data
data['signal'] = data.apply(lambda row: ge.get_signal(scy.wiener(scy.wiener(row['accel'],mysize=201),mysize=201),threshold_t=0.4,threshold_value=False),axis=1)

# Using filtered 3x data
data['signal'] = data.apply(lambda row: ge.get_signal(scy.wiener(scy.wiener(scy.wiener(row['accel'],mysize=201),mysize=201),mysize=201),threshold_t=0.4,threshold_value=False),axis=1)

# data['signal'] = data.apply(lambda row: ge.check_signal(row['accel'],row['signal'],lag=1,threshold_t=0.4,threshold_value=False),axis=1)

#data = ge.get_SNR(data,e.fs,window_type='moving')

# data = ge.get_cadence(data)

# Calculating the cadence using ALL detected steps
data['cadence'] = data.apply(lambda row: ge.get_cadence(row['accel'],row['signal'],row['time']),axis=1)

# Calculating the cadence using just 3 steps and averaging them out
data['avg_cadence'] = data.apply(lambda row: ge.get_avg_cadence(row['accel'],row['signal'],row['time']),axis=1)


#%%
c = data.drop(['accel','record','time','signal'],axis=1)



#%% Combining FEEL and PACE

