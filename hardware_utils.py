# -*- coding: utf-8 -*-
"""
Utilities for hardward
"""

def identify_channel():
    """
    Identifies the channel of the sensor that is tapped.
    
    This function collects data from all the channels available in the DAQ
    connected to the computer.  The sensor channel is identified by calculating
    the maximum amplitude of the channel.  
    
    Returns
    -------
    channel : str
        The channel in the data acquisition system
        
    Notes
    -----
    The sensitivity is set to 1 for all channels.  Therefore, the incorrect
    channel can be identified if sensors of different sensitivity are connected.
    Data is collected at 1652Hz.
    """
    import nidaqmx
    import numpy as np
    
    system = nidaqmx.system.System.local()
    
    devices = []
    channels = []
    ranges = []
    fs = 1652
    
    with nidaqmx.Task() as task:
        
        for device in system.devices:
            d_name = device.name
            devices.append(d_name)
            for chan in device.ai_physical_chans:
                c_name = chan.name
                channels.append(c_name)
                
                task.ai_channels.add_ai_accel_chan(physical_channel = c_name, sensitivity = 1, min_val = -10, max_val = 10)
                
        task.timing.cfg_samp_clk_timing(fs, samps_per_chan=fs*5,)
        data = task.read(number_of_samples_per_channel=1652*5)
        for item in data:
            r = np.max(item) - np.min(item)
            ranges.append(r)
        
    # Identify the channel with the biggest range
    index = ranges.index(max(ranges))
    return channels[index]


