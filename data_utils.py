# -*- coding: utf-8 -*-
"""
Data utils
"""
import numpy as np
import numpy

def comp_std(data):
    """ Compare std. dev. for 2000 data points at beginning and at the end
        it only analyzes the first channel available
    
        Args:
        data (numpy array): acceleration data
        
        Returns:
        decision (boolean): Decision.
    """
    b_std=np.std(data[:2000,1])
    e_std=np.std(data[-2000:,1])
    
    if e_std>b_std*1.2:
        decision=False
        print('Dismissed by comp_std criteria')
    elif b_std>e_std*1.2: #backwards comparison
        decision=False    
        print('Dismissed by comp_std criteria')
    else:
        decision=True
    
    return decision

def noise_dev(data):
    """ Calculates the std deviation and compares to given value inside
        this function
        it only analyzes the first channel available
    
        Args:
        data (numpy array): acceleration data
        value (double): Std. Dev. Threshold
        Returns:
        decision (boolean): Decision.
    """
    n_std=np.std(data[:,3])
    value=0.0023
    if n_std>value:
        decision=False
        print('Dismissed by noise std. dev. criteria')
    else:
        decision=True
    
    return decision

def noise_dev3(data):
    """ Calculates the std deviation on 3 sections of the current record
        and compares to given value inside, if all 3 values are in range
        the record gets a False decision
        
        it only analyzes the first channel available
    
        Args:
        data (numpy array): acceleration data
        value (double): Std. Dev. Threshold
        Returns:
        decision (boolean): Decision.
    """
    
    n_std1=np.std(data[:2000,3])
    n_std2=np.std(data[int((len(data[:,3])/2)-1000):int((len(data[:,3])/2)+1000),3])
    n_std3=np.std(data[-2000:,3])
    
    value=0.00105
    if n_std1>value and n_std2>value and n_std3>value:
        decision=False
        print('Dismissed by 3 point std. dev. criteria')
    else:
        decision=True
    
    
    return decision    

def rem50(data):
    """ Sorts the record from smallest to largest and removes 50% of the data
        
        it only analyzes the first channel available
    
        Args:
        data (numpy array): acceleration data
        Returns:
        decision (boolean): Decision.
    """
    channel=0
    sort=sorted(data[:,channel])
    n_std1=np.std(sort[int(len(sort)/4):int(len(sort)*3/4)])
    
    
    
    
    value=11.788079e-05 #SortedData and 50 compressor OFF.png 2+
    if n_std1>value:
        decision=False
        message='Dismissed by rem50 criteria'
        print(message)
    else:
        decision=True
    
    
    return decision    


class buffer:
    """
    Creates a buffer to be used in data collection
    #TODO: Complete help
    """
    pretrig = 0   # Number of points in the buffer for pre-trigger
    trigger = 0   # Amplitude used for trigger
    __buffer__ = []  # Buffer itself
    __position__ = 0 # Current position to add to the buffer
    
    def __init__(self,buffer_size):
        """This class is an object to serve as a buffer for data collection.
        
        Args:
            buffer_size (int): Number of points for the buffer. Corresponds to the
                number of rows
                
        Attributes:
            buffer_size (int): Number of points for the buffer.
        
        """
        self.__buffer__ =  [0] * buffer_size
        self.buffer_size = buffer_size
        
    def add(self,data):
        """Adds data to a buffer object.
        
        Args:
            data (list): List to add to the buffer.  The length data should
            be n corresponding to the number of points being added to the
            buffer.  Data can be a list of lists if more than one sensor
            will be used.
        """
        
        # Adding to the buffer
        if self.__position__ + len(data) > len(self.__buffer__):
            # If the data is bigger than the remaining of the buffer
            points_end = len(self.__buffer__) - self.__position__
            points_beginning = len(data) - points_end
            self.__buffer__[self.__position__:] = data[0:points_end]
            self.__buffer__[0:points_beginning] = data[points_end:]
            self.__position__ = points_beginning
        else:    
            # If the data is smaller than the remainder of the buffer
            self.__buffer__[self.__position__:self.__position__+len(data)] = data
            self.__position__ += len(data)

    def read(self):
        """ Reads the buffer
        
        Returns:
            data (list): Data in the correct order.
        """
        data = self.__buffer__[self.__position__:]
        for item in self.__buffer__[0:self.__position__]:
            data.append(item)
            
        return data
        


