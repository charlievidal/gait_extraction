# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 15:15:33 2021

@author: Charlie
"""
import pandas as pd
import numpy as np
import tables

def getAPDMdata():
    
    data = {'record':[], 'sensor':[], 'accel':[], 'time':[],
            'max_value':[],'best_sensor':[]}
            
            
    # Creating a specific paramters DataFrame
    df = pd.read_hdf('data\data_acquisition_p6.hdf5', 'experiment/specific_parameters')
    
    # Checking for more than one record
    record = df['id'].unique()
    
    efile = tables.open_file('data/data_acquisition_p6.hdf5', mode="r")
    edata = efile.get_node('/experiment/data')
    edata = edata.read()
    edata = edata[0:188,:,:]
    
    # Checking all trials 
    for t in range(len(edata)):
        
        # Looping through all sensors
        for s in range(np.shape(edata)[1]):
            
            data['record'].append(record[t])
            data['sensor'].append(s)
            data['accel'].append(edata[t,s,:])
            data['time'].append(np.arange(0,30,1/1652))
            data['max_value'].append(edata[t,s,np.argmax(edata[t,s,:])])
            data['best_sensor'].append(0)

    data = pd.DataFrame(data)
    
    for i in data['record'].unique():
        
        ind = np.where(data['max_value']==max(data['max_value'][data['record']==i]))[0][0]
        
        data.loc[data['record']==i,['best_sensor']] = data['sensor'][ind]
        # data['best_sensor'][data['record']==i] = data['sensor'][ind]
    
    data = data.drop(['max_value'],axis=1)
    
    efile.close()
    
    return data